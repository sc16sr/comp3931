// This sketch file is a representation of the more advanced functionality of movement.
// the aim of this sketch is to understand how the gyro is used within the device. 
// This sketch file is heavily influences from the exmaple that is provided by arduino IDE once the board has been imported.
//     some code has been directly used from the example file to have this sketch as 1 file to allow readability.
//the program being ran is to turn the device left 90 degree and then back around 270 degrees in the opposite direction

// This file is more of an experiment and play around with the exmaple file to understand howsome of the code they provide can be linked together. 
// This sketch should be ignored to all tense and purpose. 

// Commenting is note done below due to this being an experiment and will not be used later in the project

#include <Wire.h>
#include <Zumo32U4.h>

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA;
Zumo32U4ButtonA buttonB;
Zumo32U4ButtonA buttonC;
Zumo32U4Motors motors;
L3G gyro;

//setting up sensor variables
uint8_t leftSensor;
uint8_t rightSensor;
uint8_t frontRightSensor;
uint8_t frontLeftSensor;
uint16_t lineSensorValues[5];

//setting default speeds and delays
const int defaultDelay = 15;
const uint8_t defaultMovementSpeed = 150;
const uint8_t defaultMovementSpeedReverse = -150;

uint32_t turnAngle = 0;
int16_t turnRate;
int16_t gyroOffset;
uint16_t gyroLastUpdate;
int32_t total;
uint16_t i;
int32_t totalAngle;
bool buttonAPressed;
bool buttonBPressed;
bool buttonCPressed;


void setup()
{
	Wire.begin();
	gyro.init();

	turnRate = 0;
	gyroOffset = 0;
	total = 0;
	i = 0;

	gyro.writeReg(L3G::CTRL1, 0b11111111);
	gyro.writeReg(L3G::CTRL4, 0b00100000);
	gyro.writeReg(L3G::CTRL5, 0b00000000);
	
	calibrateGyro();

}

void loop()
{

	bool buttonAPressed = buttonA.getSingleDebouncedPress();
	bool buttonBPressed = buttonB.getSingleDebouncedPress();
	bool buttonCPressed = buttonC.getSingleDebouncedPress();

	if ( buttonAPressed )
	{
		delay(1000);
		turnDegrees(90);
		delay(1000);
		turnDegrees(-90);
		delay(1000);
		turnDegrees(-90);
		turnDegrees(-90);	
	}

}

void turnDegrees(int degreeToTurn)
{
	bool AngleReached = true;
	turnReset();

	Serial.println("Start");
	while( AngleReached )
	{
		turnUpdate();
		totalAngle = (((int32_t)turnAngle >> 16) * 360) >> 16;
		Serial.print(degreeToTurn); Serial.print(" | "); Serial.println(totalAngle);
		if( degreeToTurn > 0 )
		{
			motors.setSpeeds(-defaultMovementSpeed, defaultMovementSpeed);
			delay(defaultDelay);
			if( totalAngle >= degreeToTurn )
			{
				motors.setSpeeds(0,0);
				Serial.println("Completed 1");
				return;
			}
		}
		else if( degreeToTurn < 0 )
		{
			motors.setSpeeds(defaultMovementSpeed, -defaultMovementSpeed);
			delay(defaultDelay);
			if( degreeToTurn >= totalAngle )
			{
				motors.setSpeeds(0,0);
				Serial.println("Completed 2");
				return;		
			}
		}
		else
		{
			motors.setSpeeds(0,0);
			Serial.println("given degreeToTurn is 0");
			return;
		}
	}
}

void calibrateGyro()
{


	total = 0;
	for (i = 0; i < 1024; i++)
	{
		// Wait for new data to be available, then read it.
		while(!gyro.readReg(L3G::STATUS_REG) & 0x08)
		{
			//do nothing
		}

		gyro.read();

		total += gyro.g.z;
	}
	gyroOffset = total / 1024;
}

void turnUpdate()
{
  gyro.read();
  turnRate = gyro.g.z - gyroOffset;

  uint16_t newTimeTaken = micros(); //gets time since program start
  uint16_t timeSinceLastUpdate = newTimeTaken - gyroLastUpdate; //figures out how long it has been since the last update
  gyroLastUpdate = newTimeTaken; //resets the current time

  int32_t estimageTurnDegreesOverTime = (int32_t)turnRate * timeSinceLastUpdate;

  // The units of estimageTurnDegreesOverTime are gyro digits times microseconds.  We need
  // to convert those to the units of turnAngle, where 2^29 units
  // represents 45 degrees.  The conversion from gyro digits to
  // degrees per second (dps) is determined by the sensitivity of
  // the gyro: 0.07 degrees per second per digit.
  //
  // (0.07 dps/digit) * (1/1000000 s/us) * (2^29/45 unit/degree)
  // = 14680064/17578125 unit/(digit*us)
  turnAngle += (int64_t)estimageTurnDegreesOverTime * 14680064 / 17578125;
}


void turnReset()
{
	gyroLastUpdate = micros();
	turnAngle = 0;
	totalAngle = 0;
}