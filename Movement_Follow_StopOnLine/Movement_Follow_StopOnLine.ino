// This sketch file is a representation of the basic functionality of the project.
// The aim of this sketch is to go over 3 basic features of the Zumo 32u4 robot these being:
//     Movement
//     Object detection (proximity detectors)
//     Surface detection (line sensors)
// When starting the robot with this sketch uploaded, you will be able to press either button A, B or C each represent a different feature
// Button A is basic movement,
// Button B follows an object without colliding
// Button C Goes forward until the device moves onto a black surfaced
// 
 



#include <Wire.h>
#include <Zumo32U4.h>

// Basic variables that are going to be used throughout this sketch are created below
// This includes the buttons, line sensors, proximity sensor, settings (track speed, time delay), sensors value holders

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA; 
Zumo32U4ButtonB buttonB; 
Zumo32U4ButtonC buttonC;
Zumo32U4Buzzer buzzer;
Zumo32U4Motors motors;
Zumo32U4LineSensors lineSensors;
Zumo32U4ProximitySensors proxSensors;

uint8_t leftSensor;
uint8_t rightSensor;
uint8_t frontRightSensor;
uint8_t frontLeftSensor;
uint8_t defaultMovementSpeed = 150;
uint8_t defaultMovementSpeedReverse = 150;
uint16_t lineSensorValues[5];
static uint16_t timeDeleyForSensors;
static uint16_t tenSecondTimer;
uint16_t tenSeconds = 10000;
int defaultDelay = 25;

//The checkLineSensor method below, checks to see if the device has moved onto a black surface.
// If it has determined that it has, the device will move backwards, otherwise it moves forward.
void checkLineSensor()
{
   if( lineSensorValues[0] >= 750 || lineSensorValues[1] >= 750 || lineSensorValues[2] >= 750 )
  {
    motors.setSpeeds( -defaultMovementSpeed, -defaultMovementSpeed);
    delay(1000);
    motors.setSpeeds(0, 0);
    return;
  }
  else
  {
    motors.setSpeeds( defaultMovementSpeed, defaultMovementSpeed);
    delay(500);
    Serial.println("move");
    return;
  }
}

// The moveRobot method will move the device in a given direction based off of the directionToMove argument supplied.
// The device will move in said direction for the amount of time that has been provided through the timeMoving variable.
// Time is represented in MS.

// The direction that can be chosen are, forward, left, right, backwards.
// If none of these are chosen when the method is called, a message is displayed on the LCD and then returned.
void moveRobot(String directionToMove, int timeMoving)
{
  if( directionToMove.equalsIgnoreCase("forward") )
  {
    motors.setSpeeds(defaultMovementSpeed, defaultMovementSpeed);
    lcd.print(F("Forward"));  
  }
  else if( directionToMove.equalsIgnoreCase("backwards") )
  {
    motors.setSpeeds(-defaultMovementSpeed, -defaultMovementSpeed); 
    lcd.print(F("Backwards")); 
  }
  else if( directionToMove.equalsIgnoreCase("left") )
  {
    motors.setSpeeds(-defaultMovementSpeed, defaultMovementSpeed); 
    lcd.print(F("Left"));
  }
  else if( directionToMove.equalsIgnoreCase("right") )
  {
    motors.setSpeeds(defaultMovementSpeed, -defaultMovementSpeed); 
    lcd.print(F("Right"));
  }
  else
  {
    lcd.print(F("Error 1"));
    return;
  }

  delay(timeMoving);
  motors.setSpeeds(0, 0);
  return;
}

// The findObject method will move the device left or right given the argument provided.
// it will move by the defauklt time set at the beginning of the sketch file
// if neither value is provided, an error is displayed on the LCD
void findObject(String directionToMove)
{
  if(directionToMove.equals("left"))
  {
    motors.setSpeeds(-defaultMovementSpeedReverse/2, defaultMovementSpeed);
    delay(defaultDelay);      
    return;
  }
  else if(directionToMove.equals("right"))
  {
    motors.setSpeeds(defaultMovementSpeed, -defaultMovementSpeedReverse/2);
    delay(defaultDelay);
    return;
  }
  else
  {
    motors.setSpeeds(0, 0);
    lcd.gotoXY(0,0);
    lcd.print(F("ERROR 2"));
    return;
  }
}

// The updateProxSensorData method sets the variables at the beginning of the sketch  file that hold each sensors directions value
// these values represent, front left sensor, front right sensor, left sensor and right sensor
// this is then printed out in the Serial monitor and then resurned. 
void updateProxSensorData()
{
  proxSensors.read();
  
  frontLeftSensor = proxSensors.countsFrontWithLeftLeds();
  frontRightSensor = proxSensors.countsFrontWithRightLeds();
  leftSensor = proxSensors.countsLeftWithLeftLeds();
  rightSensor = proxSensors.countsRightWithRightLeds(); 


  Serial.print(proxSensors.countsLeftWithLeftLeds()); Serial.print(" "); 
  Serial.print(proxSensors.countsFrontWithLeftLeds()); Serial.print(" "); 
  Serial.print(proxSensors.countsFrontWithRightLeds()); Serial.print(" "); 
  Serial.print(proxSensors.countsRightWithRightLeds()); Serial.print(" \n");

   return;
}

// The setup method is ran at the beginnning of every run of the sketch.
// initialisation of the line and proximity sensors are performed and the delay on the prox sensors set to alter the range.
void setup() 
{
  // put your setup code here, to run once:
  lineSensors.initThreeSensors();
  proxSensors.initThreeSensors();

  proxSensors.setPulseOnTimeUs(225);
}


// the loop method is ran after setup and will be ran consistnently throughout the sketch lifetime. 
// when the final command has ran, it will loop back round to the top to run again.
// This loop is split into 3, button A, button B ,button C
// Button A will move the device forward, left, forward again, right, forward, left, forward, right and then forward in that sequence.
// button b will run infinatly, this determine what direction an object can be seen in and then will move is said direction. This is performed by using the proximity data 
void loop() 
{

  bool buttonAPressed = buttonA.getSingleDebouncedPress();
  bool buttonBPressed = buttonB.getSingleDebouncedPress();
  bool buttonCPressed = buttonC.getSingleDebouncedPress();
  motors.setSpeeds(0,0);
  lcd.gotoXY(0,0);
  lcd.print(F("A B C: "));
    
   
  //button A
  if( buttonAPressed )
  {
    lcd.clear();
    lcd.gotoXY(0,0);
    lcd.print(F("Movement: "));
    delay(1000);
    
    lcd.clear();
    moveRobot("Forward", 1500);
    lcd.clear();
//    moveRobot("Backwards", 1000);
    lcd.clear();
    moveRobot("left", 250);
    lcd.clear();
    moveRobot("forward", 1500);
//    moveRobot("right", 2000);
    lcd.clear();
    moveRobot("right", 500);
//    moveRobot("left", 1000);
    lcd.clear();
    moveRobot("forward", 1500);

   lcd.clear();
    moveRobot("Forward", 1500);
    lcd.clear();
//    moveRobot("Backwards", 1000);
    lcd.clear();
    moveRobot("left", 250);
    lcd.clear();
    moveRobot("forward", 1500);
//    moveRobot("right", 2000);
    lcd.clear();
    moveRobot("right", 500);
//    moveRobot("left", 1000);
    lcd.clear();
    moveRobot("forward", 1500);

  }
  
  //button B
  if( buttonBPressed )
  {
    tenSecondTimer = millis();
//    while( (uint16_t)(millis() - tenSecondTimer) <= 10000 )
    delay(1500);
    
    while(true)
    {     
      updateProxSensorData(); 
      lcd.clear();
      lcd.gotoXY(0,0);
      if( frontLeftSensor >= 3 || frontRightSensor >= 3 )
      {
        lcd.print(F("going forward?"));

        if( frontLeftSensor == 6 || frontRightSensor == 6)
        {
          if( frontLeftSensor > frontRightSensor )
          {
            motors.setSpeeds( -defaultMovementSpeedReverse, defaultMovementSpeed*2 );
            delay(defaultDelay);
            motors.setSpeeds(0, 0); 
          }
          else if( frontRightSensor > frontLeftSensor ) 
          {
            motors.setSpeeds( defaultMovementSpeed*2, -defaultMovementSpeedReverse );
            delay(defaultDelay);
            motors.setSpeeds(0, 0);
          }
          else
          {
            motors.setSpeeds(0, 0);
            Serial.println(" dont move ");
            delay(defaultDelay);  
          }
        }
        else if( frontLeftSensor > frontRightSensor + 1 )
        {
          motors.setSpeeds( -defaultMovementSpeedReverse, defaultMovementSpeed*1.5 );
          delay(defaultDelay);
          motors.setSpeeds(0, 0); 
        }
        else if( frontRightSensor > frontLeftSensor + 1 ) 
        {
          motors.setSpeeds( defaultMovementSpeed*1.5, -defaultMovementSpeedReverse );
          delay(defaultDelay);
          motors.setSpeeds(0, 0);
        }
        else
        {
          motors.setSpeeds(defaultMovementSpeed, defaultMovementSpeed);    
        }    
      }
      else
      {
        motors.setSpeeds(0, 0);
      }
//      else
//      {
//        lcd.clear();
//        //if there is an object to the left and right by default we go left.
//        if ( leftSensor > rightSensor ) 
//        {
//          lcd.gotoXY(0,0);
//          lcd.print(F("Left"));
//    
//          findObject("left");
//        }
//        else if ( rightSensor > leftSensor ) 
//        {
//          lcd.gotoXY(0,0);
//          lcd.print(F("Right"));    
//          
//          findObject("right");
//        }
//        else
//        {
//          //this should never prok
//          lcd.gotoXY(0,0);
//          motors.setSpeeds(0, 0);
//          delay(defaultDelay);
//          lcd.print(F("ERROR 1"));
//        }
//      }
    }
    lcd.clear();
    lcd.gotoXY(0,0);
    lcd.print(F("Time end"));
  }


 //button C

  if( buttonCPressed )
  {
    timeDeleyForSensors = millis(); 
    tenSecondTimer = millis();

    // the while loop here is to run for 10 secodns    
    while ( (uint16_t)(millis() - tenSecondTimer) <= 10000 ) 
    {
      Serial.println( (uint16_t)(millis() - tenSecondTimer));
      lcd.clear();
      
      // every 100 ms the or more the line sensors are read and then checked again.
      if ((uint16_t)(millis() - timeDeleyForSensors) >= 100)
      {
        timeDeleyForSensors = millis();
    
        lineSensors.read(lineSensorValues, true ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
    
        Serial.print(lineSensorValues[0]); Serial.print(" ");
        Serial.print(lineSensorValues[1]); Serial.print(" ");
        Serial.print(lineSensorValues[2]); Serial.println(" ");
      }
    
      checkLineSensor();
      if ( (uint16_t)(millis() - tenSecondTimer) >= tenSeconds )
      {
        Serial.println("I NEEDS RESETTING");
      }
     }
    lcd.clear();
    lcd.gotoXY(0,0);
    lcd.print(F("Time end"));
  }

}
