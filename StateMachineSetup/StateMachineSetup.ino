// This sketch file represents the initial implementation of the control system decided on (finite state machine).
// the coding for this implementation, is meant to be crude in nature as this was meant to be used as more of understanding how the control system works within this programming language as well as have something operational for unofficial battles and official battles if the final implenetation wasnt ready before this occurs.


#include <Wire.h>
#include <Zumo32U4.h>

// setting up basic Zumo variables
Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA; 
Zumo32U4ButtonB buttonB; 
Zumo32U4ButtonC buttonC;
Zumo32U4Buzzer buzzer;
Zumo32U4Motors motors;
Zumo32U4LineSensors lineSensors;
Zumo32U4ProximitySensors proxSensors;

//setting up sensor variables
uint8_t leftSensor;
uint8_t rightSensor;
uint8_t frontRightSensor;
uint8_t frontLeftSensor;
uint16_t lineSensorValues[5];

//setting default speeds and delays
const int defaultDelay = 25;
const int16_t defaultMovementSpeed = 400;
const int16_t defaultTurnSpeed = 125;

static uint16_t timeDeleyForSensors;
uint16_t leftLineSensor = 0, centreLineSensor = 0, rightLineSensor = 0;

//setting up state variables / enums
// State represents the state the device could be in within the control system 
enum State
{
	SURFACE_CHECK,
	SCAN,
	PROCESS_SCAN_DATA,
	MOVEMENT,
	WAIT
};

// Direction represents the direction the device may need to move in / the direction of an object
enum Direction
{
	FORWARD, 
	BACK, 
	LEFT, 
	RIGHT, 
	FLEFT, 
	FRIGHT, 
	STOP
};

// initilisation of enum values
enum State state;
enum Direction direction;


// Within the setup method, the sensors on the device are setup and initialised. 
// state and direction are also initialised as WAIT as the battle hasnt state for state and STOP on direction as the device should move
// LCD display shows the user what needs to happen for the device to start the battle.
// The setup then waits for the state to change ( this changes when A has been pushed on the device )
// when A has been pushed  a 5 second dealy is set to happen due to league rules.
void setup()
{
	//setting values for button pressed on zumo robot

	//initialising sensors and sensor delay/range
	lineSensors.initThreeSensors();
	proxSensors.initThreeSensors();
	proxSensors.setPulseOnTimeUs(225);

	state = WAIT;
	direction = STOP;

	lcd.gotoXY(0,0);
	lcd.print(F("WS Val"));

	lcd.gotoXY(0,1);
	lcd.print(F("Press A"));
	//check if button A is pressed
		//if so, set state to first state (scan)
		//set motor speed to 0
	//otherwise we check for button B to be pressed
		//if so, do a set movement path (function x)
		//dont set path to anything as we want A to be pressed later on

	// while( true )
	// {

		// bool buttonAPressed = buttonA.getSingleDebouncedPress();
		// bool buttonBPressed = buttonB.getSingleDebouncedPress();
		// bool buttonCPressed = buttonC.getSingleDebouncedPress();


	// the line sensors values are initially taken from when the device is switched on
	lineSensors.read(lineSensorValues, QTR_EMITTERS_ON);
	Serial.print(lineSensorValues[0]); Serial.print(" ");
	Serial.print(lineSensorValues[1]); Serial.print(" ");
	Serial.print(lineSensorValues[2]); Serial.println(" ");

		// Serial.println("LOOP 1");
	// 	if( buttonAPressed )
	// 	{
	// 		leftLineSensor = lineSensorValues[0];
	// 		centreLineSensor = lineSensorValues[1];
	// 		rightLineSensor = lineSensorValues[2];

	// 		lcd.clear();
	// 		lcd.gotoXY(0,0);
	// 		lcd.print(F("Press A"));

	// 		lcd.gotoXY(0,1);
	// 		lcd.print(F("Start"));

	// 		break;
	// 		//wait 5 seconds
	// 	}
	// }

	while( state == WAIT )
	{

		bool buttonAPressed = buttonA.getSingleDebouncedPress();

		// Serial.println("LOOP 1");
		if( buttonAPressed )
		{
			state = SURFACE_CHECK;
			motors.setSpeeds(0,0);
			delay(5000);
			//wait 5 seconds
		}
	}
}


// the control system is then looped through in the loop method. 
// the transitions should go from wait, surface check, scan, process scan data and movement. 
// the main logic is perfomed for each state in there own function. Scan is performed in the loop.
void loop()
{
	switch(state)
	{
		case WAIT:
			delay(defaultDelay);
			state = SCAN;
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("WAIT"));
			break;

		case SURFACE_CHECK:
			state = checkLineSensor();;
			break;

		case SCAN:
			// Scanning with proxy sensors
			proxSensors.read();  
			frontLeftSensor = proxSensors.countsFrontWithLeftLeds();
			frontRightSensor = proxSensors.countsFrontWithRightLeds();
			leftSensor = proxSensors.countsLeftWithLeftLeds();
			rightSensor = proxSensors.countsRightWithRightLeds(); 

			// char sensorData[50];
			// sprintf(sensorData, "%X | %X | %X | %X\n", leftSensor, frontLeftSensor, frontRightSensor, rightSensor);

			// Serial.println("Scan");
			// Serial.println(sensorData);

			state = PROCESS_SCAN_DATA;			
			break;

		case PROCESS_SCAN_DATA:
			direction = findDirectionToGo();
			state = MOVEMENT;			
			break;

		case MOVEMENT:
			performMovement(direction);		
			state = SURFACE_CHECK;	
			break;
	}
}


// the perform movemnt is setup in the same way the control system is, the direction provided will determine what action is performed
// FORWARD will move device forard by the default delay
// BACK will move the device backwards
// LEFT will rotate the device left
// RIGHT will rotate the device right 
// FLEFT will move the device forward - due to battle strat decidede upon
// FRIGHT will move the device forward - due to battle strat decidede upon
// STOP will halt the devices movement

// if none of these are performed an error is displayed in the serial monitor - this should never be the case

void performMovement(Direction direction)
{
	switch(direction)
	{
		case FORWARD:
			motors.setSpeeds(defaultMovementSpeed, defaultMovementSpeed);
			delay(defaultDelay);
			return;
		
		case BACK: 
			motors.setSpeeds(-defaultMovementSpeed, -defaultMovementSpeed);
			delay(defaultDelay);
			return;
		
		case LEFT:
			motors.setSpeeds(-defaultMovementSpeed, defaultMovementSpeed);		
			delay(defaultDelay);
			return;
		
		case RIGHT:
			motors.setSpeeds(defaultMovementSpeed, -defaultMovementSpeed);
			delay(defaultDelay);
			return;

		case FLEFT:
			motors.setSpeeds(defaultMovementSpeed, defaultMovementSpeed);		
			delay(defaultDelay);
			return;
		
		case FRIGHT: 
			motors.setSpeeds(defaultMovementSpeed, defaultMovementSpeed);
			delay(defaultDelay);
			return;
		
		case STOP:
			motors.setSpeeds(0,0);
			delay(defaultDelay);
			return;
	}
	Serial.println("something went wrong, incorrect movement type has been passed through ERROR 1");
	return;
}

// The findDirectionToGo method is to determine what direction the closest object is to itself.
// if the object is in front by a value of 2, it is checked to see if it is directly in front or in front and slight left or right
// if the object is not in front by a vlaue of 2 then left and right snesors are compared to see if either are bigger. if so the direction is set in that direction.
// otherwise the direction is set to go forward - due to battle strategy
Direction findDirectionToGo()
{

	if( frontLeftSensor >= 2 && frontRightSensor >=2 )
	{
		if( frontLeftSensor == 6 && frontRightSensor == 6 )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Forward"));
			return FORWARD;
		}
		else if( frontLeftSensor < frontRightSensor )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("F Right"));
			return FRIGHT;
		}
		else if( frontRightSensor < frontLeftSensor )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("F Left"));
			return FLEFT;
		}
		else
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Forward"));
			return FORWARD;
		}
	}
	else
	{
		if( rightSensor > leftSensor )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Right"));
			return RIGHT;
		}
		else if ( leftSensor > rightSensor )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Left"));
			return LEFT;
		}
		else
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Stop"));
			return FORWARD;
		}
	}
}


//The checkLineSensor method is used to determine if the move the device as it is seen to be on a white surface or to move to the next state 
// if the line sensor values are above 500, it is seen to be a white surface, the reverse and turn actions are performed on the device
// otehrwise the device is seen to be on black and SCAN state is set within the control system
State checkLineSensor()
{

	lineSensors.read(lineSensorValues, QTR_EMITTERS_ON);

   if( lineSensorValues[0] <= 500 || lineSensorValues[1] <= 500 || lineSensorValues[2] <= 500 )
  {
	lcd.clear();
	lcd.gotoXY(0,0);
	lcd.print(F("LINE"));
    motors.setSpeeds(-defaultMovementSpeed, -defaultMovementSpeed);
    delay(50);
    motors.setSpeeds(defaultMovementSpeed, -defaultMovementSpeed);
    delay(400);
    return SURFACE_CHECK;
  }
  else
  {
  	return SCAN;
  }
}

void performSetMovement()
{
	Serial.println("performing some action here woot woot");
}
