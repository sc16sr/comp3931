# COMP3931

Zumo robot project 

This project repository is to track progress and keep iteration throughout the year.

The Wiki page that will hold the majority of the information as i progress this year will be held in this repo located:
https://gitlab.com/sc16sr/comp3931/wikis/home

The Wiki will contain the following information:
- Homepage introducting the project
- Idea / Concepts (where updated)
- Classes implemented
- Battle Strategies

The aim of the project is to implement a control system to be evalutated against others within the project group

Uploaded the sketch files onto your own Zumo device has been explained inside the document located on the homepage of the wiki (user manual)

The folder structure is as follows:
- Movement_Follow_StopOnLine - This was the first development section of the project, to implement basic movement, line sensor and proximity sensor use
- PreciseMovement - This sketch was to use the code within RotationResist to understand how the Gyro is used to track rotation in degrees
- StateMachineSetup - This sketch hold the basic impleentation of the initial control system (finite state machine)
- StateMachineBreakdown - This folder contains the initial class files that would have been used to create the final control system implementation (movement, proximity detection, surface detection)
- TestingExtendedControlSystem - This folder holds each of the testing sketchs for each of the class implementation mentioned in the previous bullet point. This has 3 sub folders each containing a sketch of the set of tests for that class file implementation 