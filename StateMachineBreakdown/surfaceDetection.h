#ifndef surfaceDetection_h
#define surfaceDetection_h

// This class file is used to represent the line sensors functionality that the device can perform.
// Each of the public methods are to be used in order to see the surface the device is on within the final control system, to see the surface the device is on  as needed, only these method should need to be called.
// the following method are to get the sensors output
	// isOn Black|White QTREmitterOn - this method is to return to the user if the device is seen to be on a white or black surface respectivly
	// get Left|Centre|Right SensorsValues - this method is to return the respective sensor values 
// the following method are to set values within the class
	// set Black|White SurfaceValue - this method is to set the theshold values for the device being seen to be on a white or black surface
	// resetAllValues - this method is to set all the sensors values back to their default (0)
// the following methods are used to setup the device / variables used within the class
	// setupBasicSurfaceSensor - this method is used to setup the basic implementation of this class as well as set the default values within the class.

#include <Zumo32U4.h>

class surfaceDetection 
{
	public:
		//method stubs
		void setBlackSurfaceValue(int value);
		void setWhiteSurfaceValue(int value);

		boolean isOnBlackQTREmitterOn();
		boolean isOnWhiteQTREmitterOn();

		void setupBasicSurfaceSensor();

		uint16_t getLeftSensorValue();
		uint16_t getCentreSensorValue();
		uint16_t getRightSensorValue();

		void resetAllValues(); //test getSensorValues

	private: 
		//fields to be used
		int blackSurfaceValue;
		int whiteSurfaceValue;

		uint16_t lineSensorValues[5];

		Zumo32U4LineSensors lineSensors;
};


#endif