// This sketch file would be a representation of the final control system.
// Due to mitigating circumstance from lock-down this hasnt been implemented.
// The commenting on how each of these section work can be found in the StateMachineSetup sketch

#include <Wire.h>
#include <Zumo32U4.h>
#include "enum.h"

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA; 
Zumo32U4ButtonB buttonB; 
Zumo32U4ButtonC buttonC;
Zumo32U4Buzzer buzzer;
Zumo32U4Motors motors;
Zumo32U4LineSensors lineSensors;
Zumo32U4ProximitySensors proxSensors;


//setting up sensor variables
uint8_t leftSensor;
uint8_t rightSensor;
uint8_t frontRightSensor;
uint8_t frontLeftSensor;
uint16_t lineSensorValues[5];

//setting default speeds and delays
const int defaultDelay = 25;
const int16_t defaultMovementSpeed = 400;
const int16_t defaultTurnSpeed = 125;

static uint16_t timeDeleyForSensors;
uint16_t leftLineSensor = 0, centreLineSensor = 0, rightLineSensor = 0;


enum State state;
enum Direction direction;

void setup()
{
	//setting values for button pressed on zumo robot

	//initialising sensors and sensor delay/range
	lineSensors.initThreeSensors();
	proxSensors.initThreeSensors();
	proxSensors.setPulseOnTimeUs(225);

	state = WAIT;
	direction = STOP;

	lcd.gotoXY(0,0);
	lcd.print(F("WS Val"));

	lcd.gotoXY(0,1);
	lcd.print(F("Press A"));
	//check if button A is pressed
		//if so, set state to first state (scan)
		//set motor speed to 0
	//otherwise we check for button B to be pressed
		//if so, do a set movement path (function x)
		//dont set path to anything as we want A to be pressed later on

	// while( true )
	// {

		// bool buttonAPressed = buttonA.getSingleDebouncedPress();
		// bool buttonBPressed = buttonB.getSingleDebouncedPress();
		// bool buttonCPressed = buttonC.getSingleDebouncedPress();


	lineSensors.read(lineSensorValues, QTR_EMITTERS_ON);
	Serial.print(lineSensorValues[0]); Serial.print(" ");
	Serial.print(lineSensorValues[1]); Serial.print(" ");
	Serial.print(lineSensorValues[2]); Serial.println(" ");

		// Serial.println("LOOP 1");
	// 	if( buttonAPressed )
	// 	{
	// 		leftLineSensor = lineSensorValues[0];
	// 		centreLineSensor = lineSensorValues[1];
	// 		rightLineSensor = lineSensorValues[2];

	// 		lcd.clear();
	// 		lcd.gotoXY(0,0);
	// 		lcd.print(F("Press A"));

	// 		lcd.gotoXY(0,1);
	// 		lcd.print(F("Start"));

	// 		break;
	// 		//wait 5 seconds
	// 	}
	// }

	while( state == WAIT )
	{

		bool buttonAPressed = buttonA.getSingleDebouncedPress();

		// Serial.println("LOOP 1");
		if( buttonAPressed )
		{
			state = SURFACE_CHECK;
			motors.setSpeeds(0,0);
			delay(5000);
			//wait 5 seconds
		}
	}
}

void loop()
{
	switch(state)
	{
		case WAIT:
			delay(defaultDelay);
			state = SCAN;
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("WAIT"));
			break;

		case SURFACE_CHECK:
			state = checkLineSensor();;
			break;

		case SCAN:
			// Scanning with proxy sensors
			proxSensors.read();  
			frontLeftSensor = proxSensors.countsFrontWithLeftLeds();
			frontRightSensor = proxSensors.countsFrontWithRightLeds();
			leftSensor = proxSensors.countsLeftWithLeftLeds();
			rightSensor = proxSensors.countsRightWithRightLeds(); 

			// char sensorData[50];
			// sprintf(sensorData, "%X | %X | %X | %X\n", leftSensor, frontLeftSensor, frontRightSensor, rightSensor);

			// Serial.println("Scan");
			// Serial.println(sensorData);

			state = PROCESS_SCAN_DATA;			
			break;

		case PROCESS_SCAN_DATA:
			direction = findDirectionToGo();
			state = MOVEMENT;			
			break;

		case MOVEMENT:
			performMovement(direction);		
			state = SURFACE_CHECK;	
			break;
	}
}

void performMovement(Direction direction)
{
	switch(direction)
	{
		case FORWARD:
			motors.setSpeeds(defaultMovementSpeed, defaultMovementSpeed);
			delay(defaultDelay);
			return;
		
		case BACK: 
			motors.setSpeeds(-defaultMovementSpeed, -defaultMovementSpeed);
			delay(defaultDelay);
			return;
		
		case LEFT:
			motors.setSpeeds(-defaultMovementSpeed, defaultMovementSpeed);		
			delay(defaultDelay);
			return;
		
		case RIGHT:
			motors.setSpeeds(defaultMovementSpeed, -defaultMovementSpeed);
			delay(defaultDelay);
			return;

		case FLEFT:
			motors.setSpeeds(defaultMovementSpeed, defaultMovementSpeed);		
			delay(defaultDelay);
			return;
		
		case FRIGHT: 
			motors.setSpeeds(defaultMovementSpeed, defaultMovementSpeed);
			delay(defaultDelay);
			return;
		
		case STOP:
			motors.setSpeeds(0,0);
			delay(defaultDelay);
			return;
	}
	Serial.println("something went wrong, incorrect movement type has been passed through ERROR 1");
	return;
}

Direction findDirectionToGo()
{

	if( frontLeftSensor >= 2 && frontRightSensor >=2 )
	{
		if( frontLeftSensor == 6 && frontRightSensor == 6 )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Forward"));
			return FORWARD;
		}
		else if( frontLeftSensor < frontRightSensor )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("F Right"));
			return FRIGHT;
		}
		else if( frontRightSensor < frontLeftSensor )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("F Left"));
			return FLEFT;
		}
		else
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Forward"));
			return FORWARD;
		}
	}
	else
	{
		if( rightSensor > leftSensor )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Right"));
			return RIGHT;
		}
		else if ( leftSensor > rightSensor )
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Left"));
			return LEFT;
		}
		else
		{
			lcd.clear();
			lcd.gotoXY(0,0);
			lcd.print(F("Stop"));
			return FORWARD;
		}
	}
}

void performSetMovement()
{
	Serial.println("performing some action here woot woot");
}

State checkLineSensor()
{

	lineSensors.read(lineSensorValues, QTR_EMITTERS_ON);

   if( lineSensorValues[0] <= 500 || lineSensorValues[1] <= 500 || lineSensorValues[2] <= 500 )
  {
	lcd.clear();
	lcd.gotoXY(0,0);
	lcd.print(F("LINE"));
    motors.setSpeeds(-defaultMovementSpeed, -defaultMovementSpeed);
    delay(50);
    motors.setSpeeds(defaultMovementSpeed, -defaultMovementSpeed);
    delay(400);
    return SURFACE_CHECK;
  }
  else
  {
  	return SCAN;
  }
}
