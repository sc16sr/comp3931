#ifndef proximityDetection_h
#define proximityDetection_h

// This class file is used to represent the proximity sensors functionality that the device can perform.
// Each of the public methods are to be used in order to see objects within the devices range meaning withint he final control system, to see an object as needed, only these method should need to be called.
// the following method are to get the sensors output
	// getObjectDirection - this method is used to output the direction of the closest object that can be seen by the device
	// get Left|FrontLeft|FrontRight|Right SensorValues - these methods are used to return the values read by the proximity sensor back to the control system
// the following method are to set proximity values
	// setAllSensors - this method will read the proximity sensors at that moment in time and set the values within the class as neccersary
	// resetAllSensors - this method is used to set all the sensor values back to 0.
// the following methods are used to setup the device / variables used within the class
	// setupBasicProximitySensors - This method is used to setup all the default values and the class implemetnation.
	// setPulseTime - this method is used to set the delay for the proximity sensors, this is used to adjust the distance / range of the sensors can read from

#include <Zumo32U4.h>
#include "enum.h"

class proximityDetection 
{
	public:
		//method stubs
		void setPulseTime(int value);
		Direction getObjectDirection();

		void setupBasicProximitySensor();

		uint8_t getLeftSensorValue();
		uint8_t getFrontLeftSensorValue();
		uint8_t getFrontRightSensorValue();
		uint8_t getRightSensorValue();

		void setAllSensors();
		void resetAllSensors();


	private: 
		//fields to be used
		int pulseTime;

		uint8_t leftReading;
		uint8_t frontLeftReading;
		uint8_t frontRightReading;
		uint8_t rightReading;

		Zumo32U4ProximitySensors proxSensors;
};


#endif