#include "proximityDetection.h"

enum State state;
enum Direction direction;
proximityDetection proxClass;

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA; 

// LCD is cleared
// proximity class is initialised through the setupBasicProximitySensor method 
void setup()
{
	lcd.clear();
	proxClass.setupBasicProximitySensor();
}



// Each test is ran through withint the program loop
// Test 1 - check default values - expected 0 0 0 0 to be displayed on the top and bottom row
// Test 2 - check the proximity values change depending on where an object can be seen. Values dispalyed on the LCD shbould change when moved.
// Test 3 - check the direction the object is being seen in is correct. The object closest to the device should be the direction displayed. If too close it wont be picked up
// Test 4 - check the reset values - 0 0 0 0 is expected to be diaplyed on the LCD
void loop()
{
	int i = 0;
	String startTest = "Starting tests for proximity detection | Hold button A to finish test";
	
	Serial.println("Start testing proximity detection class implementation");
	Serial.println("Press A to start test suite");

	// lcd.gotoXY(0,0);
	printToLCD(startTest);	

	// while( !buttonA.getSingleDebouncedPress() ) {} // do nothing, wait
	printToLCD("Testing Default Values | Top row = Expected value | Bottom row = Actual value");

	//Test 1	
	String actualValue = String( proxClass.getLeftSensorValue() ) + " " + String( proxClass.getFrontLeftSensorValue() ) + " " + String( proxClass.getFrontRightSensorValue() ) + " " + String( proxClass.getRightSensorValue() );
	lcd.clear();
	lcd.gotoXY(0,0);
	lcd.print(F("0 0 0 0"));
	lcd.gotoXY(0,1);
	lcd.print( actualValue );

	while( !buttonA.getSingleDebouncedPress() ) {}
	lcd.clear();
	lcd.gotoXY(0,0);
	printToLCD("Testing Set Values | Top row will show values in the following format, left - front left - front right - right, these values should change as you move the device");
	
	// Test 2
	while( !buttonA.getSingleDebouncedPress() ) 
	{
		proxClass.setAllSensors();
		String actualValue = String( proxClass.getLeftSensorValue() ) + " " + String( proxClass.getFrontLeftSensorValue() ) + " " + String( proxClass.getFrontRightSensorValue() ) + " " + String( proxClass.getRightSensorValue() );
		lcd.clear();
		lcd.gotoXY(0,0);
		lcd.print(actualValue);
		delay(300);
	}
	
	lcd.clear();
	lcd.gotoXY(0,0);	
	printToLCD("Testing the get object direction | Bottom row will now contain the supposed direction of a the object");

	// Test 3
	while( !buttonA.getSingleDebouncedPress() ) 
	{
		proxClass.setAllSensors();
		String actualValue = String( proxClass.getLeftSensorValue() ) + " " + String( proxClass.getFrontLeftSensorValue() ) + " " + String( proxClass.getFrontRightSensorValue() ) + " " + String( proxClass.getRightSensorValue() );
		lcd.clear();
		lcd.gotoXY(0,0);
		lcd.print(actualValue);

		switch(proxClass.getObjectDirection())
		{
			case LEFT:
				lcd.gotoXY(0,1);
				lcd.print("LEFT");
				break;
			case FLEFT:
				lcd.gotoXY(0,1);
				lcd.print("FRONT L");
				break;
			case FRIGHT:
				lcd.gotoXY(0,1);
				lcd.print("FRONT R");
				break;
			case RIGHT:
				lcd.gotoXY(0,1);
				lcd.print("RIGHT");
				break;
			case STOP:
				lcd.gotoXY(0,1);
				lcd.print("NONE");
				break;
			case FORWARD:
				lcd.gotoXY(0,1);
				lcd.print("FORWARD");
				break;
		}
		delay(300);
	}

	lcd.clear();
	lcd.gotoXY(0,0);	
	printToLCD("Testing reset all sensor values | Top row = Expected value | Bottom row = Actual value");

	// Test 4

	proxClass.resetAllSensors();
	actualValue = String( proxClass.getLeftSensorValue() ) + " " + String( proxClass.getFrontLeftSensorValue() ) + " " + String( proxClass.getFrontRightSensorValue() ) + " " + String( proxClass.getRightSensorValue() );
	lcd.clear();
	lcd.gotoXY(0,0);
	lcd.print(F("0 0 0 0"));
	lcd.gotoXY(0,1);
	lcd.print( actualValue );
	
	while( !buttonA.getSingleDebouncedPress() ) {}

	lcd.clear();
	lcd.gotoXY(0,0);
	lcd.print("Test End");
	while( !buttonA.getSingleDebouncedPress() ) {}


}


// The printToLCD method is used to have scrollable text display on the LCD screen, This provides a way to provide user with instruction of how the test should work .
// The string to be scrolled in input through the method argument
// a variable containing spaces at the beginning and end are added onto the string provided. This is so the text starts from the right and rthe will show blank at the end of the message 
// the string is then looped through doing the following:
	// A substring is created based off the position the loop is in within the string
	// LCD is set to goto the top left
	// the substring is printed
	// delay of 300ms is set so the user has time to read
	// if the user hold the A button, text is skipped
void printToLCD(String stringToPrint)
{	
	String subString = "";
	String fullText = "        " + stringToPrint + "    ";
	int i = 0;
	while(  i < fullText.length() - 1 )
	{
		subString = fullText.substring(i, i+9);
		lcd.gotoXY(0,0);
		lcd.print(subString);
		delay(300);
		i++;
		if(buttonA.getSingleDebouncedPress())
		{
			return;
		}
	}

}