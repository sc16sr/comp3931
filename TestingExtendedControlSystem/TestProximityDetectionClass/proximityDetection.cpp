#include "proximityDetection.h"

// Pulse time is set to 225, each sensors are set to 0 by default 
// proximity sensors is initialised
// pulse time is set by default time set
// print out to the Serial setup has complete
void proximityDetection::setupBasicProximitySensor()
{
	pulseTime = 225;
	leftReading = 0;
	frontLeftReading = 0;
	frontRightReading = 0;
	rightReading = 0;

	proxSensors.initThreeSensors();
	proxSensors.setPulseOnTimeUs(pulseTime);
	Serial.println("Proximity setup complete");
}

// return the left reading variables
uint8_t proximityDetection::getLeftSensorValue()
{
	return leftReading;
}

// return the front left sensors reading variable 
uint8_t proximityDetection::getFrontLeftSensorValue()
{
	return frontLeftReading;
}

// reutnr the front right sensor reading variable
uint8_t proximityDetection::getFrontRightSensorValue()
{
	return frontRightReading;
}

// return the right sensors reading vairbale
uint8_t proximityDetection::getRightSensorValue()
{
	return rightReading;
}

// the pulse time is set by the value provided by the method argument
void proximityDetection::setPulseTime(int value)
{
	Serial.println("setting pulse time to: " + value);
	pulseTime = value;
	proxSensors.setPulseOnTimeUs(value);
}

// proximity sensors is read
// all sensor value vairables are then set based off of this reading
// print to serial the output from this reading 
void proximityDetection::setAllSensors()
{
	proxSensors.read();
	frontLeftReading = proxSensors.countsFrontWithLeftLeds();
	frontRightReading = proxSensors.countsFrontWithRightLeds();
	leftReading = proxSensors.countsLeftWithLeftLeds();
	rightReading = proxSensors.countsRightWithRightLeds(); 
	Serial.print(leftReading); Serial.print(" | "); Serial.print(frontLeftReading); Serial.print(" | "); Serial.print(frontRightReading); Serial.print(" | "); Serial.println(rightReading);
}

// The direction is based off the following condition:
//
// FORWARD is returned if left and right readings reading are less than the front left reading and the front left and front right readings are equal
// FLEFT is rturns if left, right and front right readings are less than the front left reading 
// FRIGHT is returned if left, right and front left readings are less than the front right reading
// LEFT is returned if right, front left and front right reading are less than the left reading  
// RIGHT is returned if left, front left and front right reading are less than the right reading
// STOP is returned if none of these can be determined for certain, this is when the highest sensor values are the same as another or if all sensors are set to 0
Direction proximityDetection::getObjectDirection()
{
	if( frontLeftReading > leftReading &&
		frontLeftReading > rightReading &&
		frontLeftReading == frontRightReading )
	{
		return FORWARD; 
	}
	else if( frontLeftReading > frontRightReading &&
			 frontLeftReading > leftReading &&
			 frontLeftReading > rightReading )
	{
			return FLEFT;
	}
	else if( frontRightReading > frontLeftReading &&
			 frontRightReading > leftReading &&
			 frontRightReading > rightReading )
	{
		return FRIGHT;
	}
	else if( leftReading > rightReading &&
			 leftReading > frontLeftReading &&
			 leftReading > frontRightReading )
	{	
		return LEFT;
	}
	else if( rightReading > leftReading &&
			 rightReading > frontLeftReading &&
			 rightReading > frontRightReading )
	{
		return RIGHT;
	}	
	else
	{
		return STOP;
	}

}

// all sensor variables within the class are set 0
void proximityDetection::resetAllSensors()
{
	leftReading = 0;
	frontLeftReading = 0;
	frontRightReading = 0;
	rightReading = 0;
}