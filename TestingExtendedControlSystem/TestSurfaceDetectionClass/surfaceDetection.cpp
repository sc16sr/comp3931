#include "surfaceDetection.h"

// blackSurfaceValue is set based off the argument supplied in the method call
void surfaceDetection::setBlackSurfaceValue(int value)
{
	blackSurfaceValue = value;
}

// whiteSurfaceValue is set based off the argument supplied in the method call
void surfaceDetection::setWhiteSurfaceValue(int value)
{
	whiteSurfaceValue = value;
}

// the line sensor is read
// the values are printed out to the serial (left | centre | right)
// true is returned if the sensors values read are greater tha blackSurfaceValue
// otherwise false is returned
boolean surfaceDetection::isOnBlackQTREmitterOn()
{
	lineSensors.read(lineSensorValues, QTR_EMITTERS_ON);

	Serial.println( String(lineSensorValues[0]) + " " + String(lineSensorValues[1]) + " " + String(lineSensorValues[2]));

	if( lineSensorValues[0] >= blackSurfaceValue || lineSensorValues[1] >= blackSurfaceValue || lineSensorValues[2] >= blackSurfaceValue )
	{
		return true;
	}
	else
	{
		return false;
	}
}

// the line sensor is read
// the values are printed out to the serial (left | centre | right)
// true is returned if the sensors values read are less tha whiteSurfaceValue
// otherwise false is returned
boolean surfaceDetection::isOnWhiteQTREmitterOn()
{
	lineSensors.read(lineSensorValues, QTR_EMITTERS_ON);

	Serial.println( String(lineSensorValues[0]) + " " + String(lineSensorValues[1]) + " " + String(lineSensorValues[2]));

	if( lineSensorValues[0] <= whiteSurfaceValue || lineSensorValues[1] <= whiteSurfaceValue || lineSensorValues[2] <= whiteSurfaceValue )
	{
		return true;
	}
	else
	{
		return false;
	}
}

// the line sensors is initialised
// the black and white surface value is set to 500 by default
// line sensors valiues are set to 0 by default
void surfaceDetection::setupBasicSurfaceSensor()
{
	lineSensors.initThreeSensors();

	blackSurfaceValue = 500;
	whiteSurfaceValue = 500;

	lineSensorValues[0] = 0;
	lineSensorValues[1] = 0;
	lineSensorValues[2] = 0;
	lineSensorValues[3] = 0;
	lineSensorValues[4] = 0;
}

// the left sensor value is returned
uint16_t surfaceDetection::getLeftSensorValue()
{
	return lineSensorValues[0];
}

// the centre sensor value is returned
uint16_t surfaceDetection::getCentreSensorValue()
{
	return lineSensorValues[1];
}

// the right sensor value is returned
uint16_t surfaceDetection::getRightSensorValue()
{
	return lineSensorValues[2];
}

// all the sensors values are set to 0 
void surfaceDetection::resetAllValues()
{
	lineSensorValues[0] = 0;
	lineSensorValues[1] = 0;
	lineSensorValues[2] = 0;
	lineSensorValues[3] = 0;
	lineSensorValues[4] = 0;
}