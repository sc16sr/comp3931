#include "surfaceDetection.h"

surfaceDetection surfaceClass;

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA; 

// lcd screen is cleared
// surface detection class is initialised through the setupBasicSurfaceSensor method
void setup()
{
	lcd.clear();
	surfaceClass.setupBasicSurfaceSensor();
}


// Each test is ran through withint the program loop
// Test 1 - check default values - expected to display true on the LCD
// Test 2 - check the surface detection works for the device beign on a black surface
	// isOnBlackQTREmitterOn expected to be true - isOnWhiteQTREmitterOn expected to be false. True is disaplyed if this is the case
	// false otherwise
// Test 3 - check the surface detection works on the device being on a white surface. 
	// Setup in the same manor as the previous test
// Test 4 - check reset values - expected to dispaly true on the LCD
// Test finsih and loop back round is A is pressed again
void loop()
{	
	String startTest = "Starting tests for surface detection | Hold button A to finish test";
	String defaultTest = "Testing default setup | Screen should show true";
	String onBlackTest = "Testing on black surface | Place device on black surface | Screen should show true";
	String onWhiteTest = "Testing on white surface | Place device on white surface | Screen should show true";
	String onResetTest = "Testing reset values | Screen should show true";

	Serial.println("Start testing surface detection class implementation");
	Serial.println("Press A to start test suite");

	printToLCD(startTest);	
	printToLCD(defaultTest);

	// Test 1

	if( surfaceClass.getLeftSensorValue() == 0 &&
		surfaceClass.getCentreSensorValue() == 0 &&
		surfaceClass.getRightSensorValue() == 0)
	{
		lcd.clear();
		lcd.print("True");
	}
	else
	{
		lcd.clear();
		lcd.print("False");
	}

	while( !buttonA.getSingleDebouncedPress() ) {}

	printToLCD(onBlackTest);

	// Test 2
	
	if( surfaceClass.isOnBlackQTREmitterOn() &&
		!surfaceClass.isOnWhiteQTREmitterOn() )
	{
		lcd.clear();
		lcd.print("True");
	}
	else
	{
		lcd.clear();
		lcd.print("False");		
	}

	while( !buttonA.getSingleDebouncedPress() ) {}

	printToLCD(onWhiteTest);

	// Test 3
	
	if( surfaceClass.isOnWhiteQTREmitterOn() && 
		   !surfaceClass.isOnBlackQTREmitterOn() )
	{
		lcd.clear();
		lcd.print("True");
	}
	else
	{
		lcd.clear();
		lcd.print("False");		
	}

	while( !buttonA.getSingleDebouncedPress() ) {}

	printToLCD(onResetTest);

	// Test 4

	surfaceClass.resetAllValues();
	if( surfaceClass.getLeftSensorValue() == 0 &&
		surfaceClass.getCentreSensorValue() == 0 &&
		surfaceClass.getRightSensorValue() == 0)
	{
		lcd.clear();
		lcd.print("True");
	}
	else
	{
		lcd.clear();
		lcd.print("False");
	}	


	while( !buttonA.getSingleDebouncedPress() ) {}

	lcd.clear();
	lcd.gotoXY(0,0);
	lcd.print("Test End");

	while( !buttonA.getSingleDebouncedPress() ) {}


}

// The printToLCD method is used to have scrollable text display on the LCD screen, This provides a way to provide user with instruction of how the test should work .
// The string to be scrolled in input through the method argument
// a variable containing spaces at the beginning and end are added onto the string provided. This is so the text starts from the right and rthe will show blank at the end of the message 
// the string is then looped through doing the following:
	// A substring is created based off the position the loop is in within the string
	// LCD is set to goto the top left
	// the substring is printed
	// delay of 300ms is set so the user has time to read
	// if the user hold the A button, text is skipped
void printToLCD(String stringToPrint)
{	
	String subString = "";
	String fullText = "        " + stringToPrint + "    ";
	int i = 0;
	while(  i < fullText.length() - 1 )
	{
		subString = fullText.substring(i, i+9);
		lcd.gotoXY(0,0);
		lcd.print(subString);
		delay(300);
		i++;
		if(buttonA.getSingleDebouncedPress())
		{
			return;
		}
	}

}