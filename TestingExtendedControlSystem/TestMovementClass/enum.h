#ifndef enum_h
#define enum_h

//setting up state variables / enums


// this is the control system states for the loop() method (changes these)
enum State
{
	SURFACE_CHECK,
	SCAN,
	PROCESS_SCAN_DATA,
	MOVEMENT,
	WAIT
};


//this is the emum states for the objects files (keep these the same)
enum Direction
{
	FORWARD, 
	BACK, 
	LEFT, 
	RIGHT, 
	FLEFT, 
	FRIGHT, 
	STOP
};

enum battleStratagies
{
	ATTACK,
	DEFEND,
	STALL
};



#endif