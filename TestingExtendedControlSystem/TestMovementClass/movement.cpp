#include "movement.h"

//if directioon is set to left, the device turn left.
//if direction is set to right, the device turns right.
//otherwise nothing happened
void movement::turnByTime(int lengthOfTime, String direction)
{
	direction.toLowerCase();
	if( direction.equals("left") )
	{
		motors.setSpeeds(reverseSpeed, forwardSpeed);
		delay(lengthOfTime);
		motors.setSpeeds(0,0);
		return;
	}
	else if( direction.equals("right") )
	{
		motors.setSpeeds(forwardSpeed, reverseSpeed);
		delay(lengthOfTime);
		motors.setSpeeds(0,0);
		return;
	}
}

// The degrees value is checked to see if it is within range. If not then nothing happens 
// otherwise, we check if the device need to turn left or right
// if the degrees is greater than 0 then we turn left
// otherwise we turn right
// inside each, we set the speeds of the tracks and then perform the following loop
// while the angle the device is at is not passed the degrees set 
	// dely and update the sensor values.
// once the device reaches the degrees needed, the device is stopped and then returned 
void movement::turnByDegrees(int degrees)
{
	gyroLastUpdate = micros();
	turnAngle = 0;

	int angle = (((int32_t)turnAngle >> 16) * 360) >> 16;
	lcd.gotoXY(0, 0);
	lcd.print(angle);
	lcd.print(" ");
	
	if( degrees > 179 || degrees < -180 )
	{
		return;
	}
	else
	{
		//turn left
		if( degrees > 0 )
		{
			motors.setSpeeds(reverseSpeed, forwardSpeed);
			while( angle < degrees )
			{
				delay(1);
				gyroSensorUpdate();
				angle = (((int32_t)turnAngle >> 16) * 360) >> 16;
				lcd.gotoXY(0, 0);
				lcd.print(angle);
				lcd.print(" ");
			}
			motors.setSpeeds(0,0);
		}
		//turn right
		else
		{
			motors.setSpeeds(forwardSpeed, reverseSpeed);
			while( angle > degrees )
			{
				delay(1);
				gyroSensorUpdate();
				angle = (((int32_t)turnAngle >> 16) * 360) >> 16;
			    lcd.gotoXY(0, 0);
			    lcd.print(angle);
			    lcd.print(" ");
			}
			motors.setSpeeds(0,0);
		}
	}
	
}

// The left and rigt encoder values the track speeds are set
// we perform the forward movement until both left and right encoder values are greater than the value provided in the method argument
// once this has been reached, the device stops and method is returned
void movement::moveForwardByEncoderAmount(int16_t distance)
{

	leftEncoder = encoders.getCountsAndResetLeft();
	rightEncoder = encoders.getCountsAndResetRight();

	motors.setSpeeds(forwardSpeed,forwardSpeed);
	
	while(true)
	{
		leftEncoder = encoders.getCountsLeft();
		rightEncoder = encoders.getCountsRight();
		if( leftEncoder > distance && 
		    rightEncoder > distance )
		{
			motors.setSpeeds(0,0);
			return;		
		}
	}
}

// The left and rigt encoder values the track speeds are set
// we perform the backwards movement until both left and right encoder values are greater than the value provided in the method argument
// once this has been reached, the device stops and method is returned
void movement::moveBackwardByEncoderAmount(int16_t distance)
{
	leftEncoder = encoders.getCountsAndResetLeft();
	rightEncoder = encoders.getCountsAndResetRight();

	motors.setSpeeds(reverseSpeed,reverseSpeed);
	
	while(true)
	{
		leftEncoder = encoders.getCountsLeft();
		rightEncoder = encoders.getCountsRight();
		if( leftEncoder < distance && 
		    rightEncoder < distance )
		{
			motors.setSpeeds(0,0);
			return;		
		}
	}
}

//the forward speeds variable is set based on the input provided
void movement::setForwardSpeed(int speed)
{
	forwardSpeed = speed;
}

// the reverse speed variable is set based on the input provided
void movement::setReverseSpeed(int speed)
{
	reverseSpeed = speed;
}

// left and right encoder variables are set based on the output from getCount.. from the motors library
void movement::setEncoderValues()
{
		leftEncoder = encoders.getCountsLeft();
		rightEncoder = encoders.getCountsRight();
}

// left and right encoders are set by getCountsAndReset.. from the motoros library
void movement::resetEncoderValues()
{
		leftEncoder = encoders.getCountsAndResetLeft();
		rightEncoder = encoders.getCountsAndResetRight();	
}

// left encoder value is returned by the getCountLeft method from the motors library
int16_t movement::getLeftEncoder()
{
	return encoders.getCountsLeft();
}

// right encoder value is returned by the getCountRight method from the motors library
int16_t movement::	getRightEncoder()
{
	return encoders.getCountsRight();
}

// All the class variables are set to their default values
// the gyro registers are created within the device
// and the calibration for the gyro is performed
void movement::setupBasicMovement() //setup encoder values, forward and reverse speed by default
{
	forwardSpeed = 0;
	reverseSpeed = 0;

	
	encoders.init();
	leftEncoder = encoders.getCountsAndResetLeft();
	rightEncoder = encoders.getCountsAndResetRight();

	motors.setSpeeds(0,0);

	//Setting up gyro
	turnAngle = 0;
	gyroLastUpdate = 0;
	
	Wire.begin();	
	gyro.init();

 	gyro.writeReg(L3G::CTRL1, 0b11111111);
  	gyro.writeReg(L3G::CTRL4, 0b00100000);
	gyro.writeReg(L3G::CTRL5, 0b00000000);

	lcd.clear();
	lcd.print(F("Gyro cal"));
	ledYellow(1);

	delay(500);

	int32_t total = 0;
	for (uint16_t i = 0; i < 1024; i++)
	{
		while(!gyro.readReg(L3G::STATUS_REG) & 0x08);
		gyro.read();

		total += gyro.g.z;
	}
	lcd.clear();
	lcd.print(F("OUT"));
	ledYellow(0);
	gyroOffset = total / 1024;


}

// the gyro values are set on the device.
// the turn rate is then calulcated by the output of this by the offset that is set in the callibration
// time is then factored into the update
// the degrees that has changed since the last update is then calculated and the degrees is set through previous calulations 
void movement::gyroSensorUpdate()
{
  gyro.read();
  turnRate = gyro.g.z - gyroOffset;

  uint16_t currentTime = micros();
  uint16_t timePassed = currentTime - gyroLastUpdate;
  gyroLastUpdate = currentTime;

  int32_t degreeChanged = (int32_t)turnRate * timePassed;
  turnAngle += (int64_t)degreeChanged * 14680064 / 17578125;
}

// the method is setup in the same way as the control system if the state provided is:
// FORWARD - the device move forward 
// BACK - the device move backwards
// LEFT - the device turn left
// RIGTH - the device turns right
// FLEFT - the device goes forward and left 
// FRIGHT - the device goes forward and right
// STOP - the device stops
void movement::moveByTime(int lengthOfTime, Direction state)
{
	switch(state)
	{
		case FORWARD:
			motors.setSpeeds(forwardSpeed, forwardSpeed);
			delay(lengthOfTime);
			motors.setSpeeds(0,0);
			return;
		
		case BACK:
			motors.setSpeeds(reverseSpeed, reverseSpeed);
			delay(lengthOfTime);
			motors.setSpeeds(0,0);
			return;
		
		case LEFT:
			motors.setSpeeds(reverseSpeed, forwardSpeed);
			delay(lengthOfTime);
			motors.setSpeeds(0,0);
			return;
		
		case RIGHT:
			motors.setSpeeds(forwardSpeed, reverseSpeed);
			delay(lengthOfTime);
			motors.setSpeeds(0,0);
			return;
		
		case FLEFT:
			motors.setSpeeds((reverseSpeed/3), forwardSpeed);
			delay(lengthOfTime);
			motors.setSpeeds(0,0);
			return;
		
		case FRIGHT: 
			motors.setSpeeds(forwardSpeed, (reverseSpeed/3));
			delay(lengthOfTime);
			motors.setSpeeds(0,0);
			return;
		
		case STOP:
			motors.setSpeeds(0, 0);
			delay(lengthOfTime);
			motors.setSpeeds(0,0);
			return;	
	}

}