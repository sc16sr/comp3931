#include "movement.h"

enum State state;
enum Direction direction;
movement moveClass;

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA; 

// move class is initialised through the setupBasicMovement method
// forward and reverse speeds set to 200 and -200 
// lcd is cleared
void setup()
{
	moveClass.setupBasicMovement();
	moveClass.setForwardSpeed(200);
	moveClass.setReverseSpeed(-200);
	lcd.clear();
}

// Each test is ran through withint the program loop
// Test 1 - check default values - this is expected to return true, left and right encoder values should be set to 0
// Test 2 - check to see the device and turn left and right for a specified amount of time - 1 second
// Test 3 - check the device can turn by number of degrees - 45 degree on the left and right
// Test 4 - check the device can turn again by number of degrees - 90 degrees on left adnt right
// Test 5 - check the device can turn to the max turn radius of the device - 179 degree left and 180 to the right
// Test 6 - check the device can move forward and backwards by specified amount of time - 1 second approx.
// Test 7 - check the device can move forward by a speicifed encoder value - 1000 - values displayed in LCD should be greater than 1000
// Test 8 - check the device can move backwards by a speicifed encoder value - -1000 - values displayed in LCD should be less than -1000
// Test 9 - check the reset values for the encoder amounts - values dispalyed on the LCD should be 0

void loop()
{
 	
	// String startTest = "Starting tests for movement | Hold button A to finish test";
	// String defaultTest = "Testing default setup | Screen should show true";
	// String turnByTime = "Testing turn movement by time | Device should turn left for 1 second and then turn right for another";
	// String turnBy45Degree = "Testing turn movement by degrees | Device should turn left and then right for 45 degrees";
	// String turnBy90Degree = "Testing turn movement by degrees | Device should turn left and then right for 90 degrees";
	// String turnBy180Degree = "Testing turn movement by degrees | Device should turn left and then right for 180 degrees";
	// String moveByTime = "Testing forward and backward movement by time | Device should move forward for 1 second and then reverse for another";
	// String moveByEncoder = "Testing forward and backward movement by encoder count | Top row should show true (forward movement) | Bottom row should show true (reverse movement)";
	// String resetEncoderValues = "Testing encoder count reset | Top row should show true";

	lcd.clear();
	// lcd.print("Start");
	printToLCD("Starting tests for movement | Hold button A to finish test");
	printToLCD("Testing default setup | Screen should show true");

	lcd.clear();

	// Test 1

	if(moveClass.getLeftEncoder() == 0 && moveClass.getRightEncoder() == 0)
	{
		lcd.print("true");
	}
	else
	{
		lcd.print(moveClass.getLeftEncoder());
		lcd.gotoXY(0,1);
		lcd.print(moveClass.getRightEncoder());
	}

	while( !buttonA.getSingleDebouncedPress() ) {}

	lcd.clear();

	printToLCD("Testing turn movement by time | Device should turn left for 1 second and then turn right for another");

	// Test 2

	delay(500);

	moveClass.turnByTime(1000, "left");

	delay(500);

	moveClass.turnByTime(1000, "right");

	while( !buttonA.getSingleDebouncedPress() ) {}
	lcd.clear();

	printToLCD("Testing turn movement by degrees | Device should turn left and then right for 45 degrees");

	// Test 3

	delay(500);

	moveClass.turnByDegrees(45);

	delay(500);

	moveClass.turnByDegrees(-45);


	while( !buttonA.getSingleDebouncedPress() ) {}
	lcd.clear();

	printToLCD("Testing turn movement by degrees | Device should turn left and then right for 90 degrees");

	// Test 4

	delay(500);

	moveClass.turnByDegrees(90);

	delay(500);

	moveClass.turnByDegrees(-90);


	while( !buttonA.getSingleDebouncedPress() ) {}
	lcd.clear();

	printToLCD("Testing turn movement by degrees | Device should turn left and then right for 180 degrees");

	// Test 5

	delay(500);

	moveClass.turnByDegrees(179);

	delay(500);

	moveClass.turnByDegrees(-180);

	while( !buttonA.getSingleDebouncedPress() ) {}
	lcd.clear();

	printToLCD("Testing forward and backward movement by time | Device should move forward for 1 second and then reverse for another");

	delay(500);

	// Test 6

	moveClass.moveByTime(1000, FORWARD);

	delay(500);

	moveClass.moveByTime(1000, BACK);


	while( !buttonA.getSingleDebouncedPress() ) {}
	lcd.clear();

	printToLCD("Testing forward and backward movement by encoder count | Top row should show true (forward movement) | Bottom row should show true (reverse movement)");

	// Test 7

	delay(500);

	moveClass.moveForwardByEncoderAmount(1000);

	lcd.clear();
	lcd.print(moveClass.getLeftEncoder());
	lcd.gotoXY(0,1);
	lcd.print(moveClass.getRightEncoder());


	while( !buttonA.getSingleDebouncedPress() ) {}

	delay(500);

	// Test 8

	moveClass.moveBackwardByEncoderAmount(-1000);

	lcd.clear();
	lcd.print(moveClass.getLeftEncoder());
	lcd.gotoXY(0,1);
	lcd.print(moveClass.getRightEncoder());	

	while( !buttonA.getSingleDebouncedPress() ) {}
	lcd.clear();

	printToLCD("Testing encoder count reset | Top row should show true");

	moveClass.resetEncoderValues();

	// Test 9
	
	lcd.clear();
	if(moveClass.getLeftEncoder() == 0 && moveClass.getRightEncoder() == 0)
	{
		lcd.print("true");
	}
	else
	{
		lcd.print(moveClass.getLeftEncoder());
		lcd.gotoXY(0,1);
		lcd.print(moveClass.getRightEncoder());
	}

	while( !buttonA.getSingleDebouncedPress() ) {}


}



// The printToLCD method is used to have scrollable text display on the LCD screen, This provides a way to provide user with instruction of how the test should work .
// The string to be scrolled in input through the method argument
// a variable containing spaces at the beginning and end are added onto the string provided. This is so the text starts from the right and rthe will show blank at the end of the message 
// the string is then looped through doing the following:
	// A substring is created based off the position the loop is in within the string
	// LCD is set to goto the top left
	// the substring is printed
	// delay of 300ms is set so the user has time to read
	// if the user hold the A button, text is skipped
void printToLCD(String stringToPrint)
{	
	String subString = "";
	String fullText = "        " + stringToPrint + "    ";
	int i = 0;
	while(  i < fullText.length() - 1 )
	{
		subString = fullText.substring(i, i+9);
		lcd.gotoXY(0,0);
		lcd.print(subString);
		delay(300);
		i++;
		if(buttonA.getSingleDebouncedPress())
		{
			return;
		}
	}
}