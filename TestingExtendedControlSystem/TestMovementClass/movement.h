#ifndef movement_h
#define movement_h

// This class file is used to represent the movement functionality that the device can perform.
// Each of the public methods are to be used in order to move the device meaning withint he final control system, to move the device as needed, only these method should need to be called.
// the following method are to be moved by time provded:
	// turnByTime - this method is to turn left or right by time
	// moveByTime - this method is to perform a movement by time (forward left, forward right, left, right, forward, backwards)
// the following method are to be moved by a specific figure provided
	// turnByDegrees - this is to turn the device by a specific amount of degrees, 179 to the left to -180 (- to the right)
	// move Forward/Backwards ByEncoderAmount - this is to move the device based off of the encoder amout provided
// the following methods are used to setup the device / variables used within the class
	// set Forward/Reverse Speeds - this sets the speed of the device when moving, forward should be a positive number up to 400 and reverse should be a negative number up to -400
	// resetEncoderValues - this is to reset the values within the class that represent the encoders.
	// setupBasicMovement - this is to setup the default implemetnation of this class, this will calibrate the gyro used for turnBYDegrees and will setup the encoders and motors within the device.

// gyroSensorUpdate is a private class as  it is used to update the angle the device has rotated. this is not requried to be used outside of the class implementation.

#include <Wire.h>
#include <Zumo32U4.h>
#include "enum.h"

class movement 
{
	public:
		//method stubs
		void turnByTime(int lengthOfTime, String direction);
		void turnByDegrees(int degrees);
		void moveForwardByEncoderAmount(int16_t distance);
		void moveBackwardByEncoderAmount(int16_t distance);

		void setForwardSpeed(int speed);
		void setReverseSpeed(int speed);
		void setEncoderValues();
		void resetEncoderValues();
		void setupBasicMovement(); //setup encoder values, forward and reverse speed by default

		int16_t getLeftEncoder();
		int16_t getRightEncoder();

		void moveByTime(int lengthOfTime, Direction state);

	private: 
		//fields to be used
		int forwardSpeed;
		int reverseSpeed;
		
		Zumo32U4Encoders encoders;
		int16_t leftEncoder;
		int16_t rightEncoder;

		Zumo32U4Motors motors;
		L3G gyro;

		Zumo32U4LCD lcd;

		void gyroSensorUpdate();
		int16_t turnRate;
		int16_t gyroOffset;
		uint32_t turnAngle;
		uint16_t gyroLastUpdate;
};


#endif